EFFECT.matRefract = Material("space_combat/shield")
EFFECT.matLight = Material("space_combat/shield")

function EFFECT:Init(data)
	local ent = data:GetEntity()
	if !IsValid(ent) then return end

	local color = data:GetColor()

	self.Color = {}
	self.Color.r = bit.band(color, 1) * 255
	self.Color.g = bit.band(color, 2) * 255
	self.Color.b = bit.band(color, 4) * 255

	self.Time = 1
	self.LifeTime = CurTime() + self.Time

	self.Parent = ent
	self.Entity:SetModel(self.Parent:GetModel())
	self.Entity:SetPos(self.Parent:GetPos())
	self.Entity:SetAngles(self.Parent:GetAngles())
	self.Entity:SetParent(self.Parent)
end

function EFFECT:Think()
	if !IsValid(self.Parent) then return false end
	return self.LifeTime > CurTime()
end
	
function EFFECT:Render()
	local Fraction = math.Clamp((self.LifeTime - CurTime()) / self.Time, 0, 1)
	self.Entity:SetColor(Color(self.Color.r, self.Color.g, self.Color.b, 1 + math.sin(Fraction * math.pi) * 100))
	self.Entity:SetRenderMode(RENDERMODE_TRANSALPHA)

	local EyeNormal = self.Entity:GetPos() - EyePos()
	local Dist = EyeNormal:Length()
	EyeNormal:Normalize()
	
	local Pos = EyePos() + EyeNormal * Dist * 0.01

	cam.Start3D(Pos, EyeAngles())
		render.MaterialOverride(self.matLight)
			self.Entity:DrawModel()
		render.MaterialOverride(0)

		if (render.GetDXLevel() >= 80) then
			render.UpdateRefractTexture()
			self.matRefract:SetFloat("$refractamount", Fraction ^ 2)

			render.MaterialOverride(self.matRefract)
				self.Entity:DrawModel()
			render.MaterialOverride(0)
		end
	cam.End3D()
end