TOOL.Category		= "Space Combat"
TOOL.Name			= "Ship Core"
TOOL.Command		= nil
TOOL.ConfigName		= ""

if CLIENT then
	language.Add("Tool.sc_core.name", "Ship Core Creation Tool")
	language.Add("Tool.sc_core.desc", "Turns a prop into a Ship Core.")
	language.Add("Tool.sc_core.0", "Primary: Create Ship Core")
	language.Add("sboxlimit_sc_core", "You've hit the Ship Core limit!")
	language.Add("undone_sc_core", "Undone Ship Core")
	language.Add("Cleanup_sc_core", "Ship Cores")
	language.Add("Cleaned_sc_core", "Cleaned all ship cores!")

	function TOOL.BuildCPanel(panel)
		panel:AddControl("Header", { Text = "#Tool.sc_core.name", Description = "#Tool.sc_core.desc" })
		panel:AddControl("CheckBox", {  Label = "Keep original constraints", Command = "sc_keep_constraints" })
	end
end

if SERVER then
	CreateConVar("sbox_maxsc_core", 3)
	CreateConVar("sc_keep_constraints", 1)
end

cleanup.Register("sc_core")

function TOOL:LeftClick(trace)
	local ent = trace.Entity
	if !IsValid(ent) then return false end
	if ent:IsPlayer() or ent:IsWorld() or ent:GetClass() != "prop_physics" then return false end

	if CLIENT then return true end

	if !self:GetSWEP():CheckLimit("sc_core") then return false end
	local ply = self:GetOwner()
	if table.HasValue(SC_ModelBlacklist, ent:GetModel()) then
		ply:SendLua("GAMEMODE:AddNotify(\"That model is blacklisted!\", NOTIFY_ERROR, 5)")
		return false
	end

	local core = ents.Create("ship_core")
	core:SetPos(ent:GetPos())
	core:SetAngles(ent:GetAngles())
	core:SetModel(ent:GetModel())
	core:Spawn()
	core.SC_Owner = ply
	core:Activate()

	local phys = ent:GetPhysicsObject()
	local corephys = core:GetPhysicsObject()
	if(phys and phys:IsValid() and corephys and corephys:IsValid()) then
		corephys:EnableMotion(phys:IsMotionEnabled())		
	end

	if(GetConVar("sc_keep_constraints"):GetBool()) then
		sc_debug("Recreating original constraints")

		for _,c in pairs(constraint.GetTable(ent)) do
			local t = c.Type
			if(t == "Weld") then
				if(c.Ent1 == ent) then
					constraint.Weld(core, c.Ent2, 0, c.Bone2, c.forcelimit, c.nocollide)
				else
					constraint.Weld(c.Ent1, core, c.Bone1, 0, c.forcelimit, c.nocollide)
				end
			else
				sc_debug("Unknown constraint: " .. t)
			end
		end
		
		--[[
		// Experimental constraint code
		if(ent.Constraints) then		
			for i=1, 6 do
				if(ent.ConstraintSystem.UsedEntities[i] and ent.ConstraintSystem.UsedEntities[i] == ent) then
					ent.ConstraintSystem.UsedEntities[i] = core
				end
			end

			for _,const in pairs(ent.Constraints) do
				if(const) then
					local tbl = const:GetTable()
					if(tbl) then
						for i=1, 6 do
							if(tbl["Ent"..i] and tbl["Ent"..i] == ent) then
								tbl["Ent"..i] = core
							end
						end
						const:SetTable(tbl)
					end
				end
			end

			core.ConstraintSystem = ent.ConstraintSystem
			core.Constraints = ent.Constraints

			sc_debug_table(ent.ConstraintSystem:GetTable())
			sc_debug_table(ent.Constraints[1]:GetTable())
		end
		]]
	else
		sc_debug("Removing original constraints")
	end

	constraint.RemoveAll(ent)
	ent:Remove()


	ply:AddCount("sc_core", core)
	ply:AddCleanup("sc_core", core)
	undo.Create("sc_core")
		undo.AddEntity(core)
		undo.SetPlayer(ply)
	undo.Finish()

	sc_debug("Created new core")
	sc_debug("Player has " .. ply:GetCount("sc_core") .. "/" .. GetConVar("sbox_maxsc_core"):GetInt()  .. " cores: " .. tostring(self:GetSWEP():CheckLimit("sc_core")))
	return true
end