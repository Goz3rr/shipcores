TOOL.Category		= "Space Combat"
TOOL.Name			= "Immunity"
TOOL.Command		= nil
TOOL.ConfigName		= ""

if CLIENT then
	language.Add("Tool.sc_immune.name", "Space Core Immunity")
	language.Add("Tool.sc_immune.desc", "Edit an entity's immunity")
	language.Add("Tool.sc_immune.0", "Primary: Make prop immune, Secondary: Make prop vulnerable, Reload: Reset prop health")

	function TOOL.BuildCPanel(panel)
		panel:AddControl("Header", { Text = "#Tool.sc_immune.name", Description = "#Tool.sc_immune.desc" })
	end	
end

function TOOL:DrawHUD()
	local ply = self:GetOwner()
	for _,ent in pairs(ents.FindInSphere(ply:GetPos(), 2048)) do
		if IsValid(ent) then
			local percent = 1
			local hp = "?"
			local maxhp = "?"
			if ent:GetNetworkedFloat("SC_Health") then
				percent = math.Round(ent:GetNetworkedFloat("SC_Health") / ent:GetNetworkedFloat("SC_MaxHealth"))
				hp = math.Round(ent:GetNetworkedFloat("SC_Health"))
				maxhp = math.Round(ent:GetNetworkedFloat("SC_MaxHealth"))
			end
			local pos = ent:GetPos():ToScreen()
			local dist = ent:GetPos():Distance(ply:GetShootPos())

			local text = hp .. "/" .. maxhp
			if ent:GetNetworkedBool("SC_Immune") then text = text .. " (Immune)" end

			surface.SetFont("ScoreboardText")
			local length = surface.GetTextSize(text)
			draw.WordBox(6, pos.x - length / 2, pos.y + 20, text, "ScoreboardText", Color(255 * (1-percent), 255 * percent, 0, math.Clamp(2048-dist, 0, 255)), Color(50, 50, 50, math.Clamp(2048-dist, 0, 255)))
		end
	end
end

function TOOL:LeftClick(trace)
	local ent = trace.Entity
	if !IsValid(ent) then return false end
	if ent:IsPlayer() or ent:IsWorld() then return false end

	if CLIENT then return true end

	local ply = self:GetOwner()
	if !ply:IsSuperAdmin() then return false end

	SC_SetImmune(ent, true)

	return true
end

function TOOL:RightClick(trace)
	local ent = trace.Entity
	if !IsValid(ent) then return false end
	if ent:IsPlayer() or ent:IsWorld() then return false end

	if CLIENT then return true end

	local ply = self:GetOwner()
	if !ply:IsSuperAdmin() then return false end

	SC_SetImmune(ent, false)

	return true
end

function TOOL:Reload(trace)
	local ent = trace.Entity
	if !IsValid(ent) then return false end
	if ent:IsPlayer() or ent:IsWorld() then return false end

	if CLIENT then return true end

	local ply = self:GetOwner()
	if !ply:IsSuperAdmin() then return false end

	if ent.SC_MaxHealth then
		SC_SetHealth(ent, ent.SC_MaxHealth)
	else
		SC_InitEnt(ent)
	end

	return true
end