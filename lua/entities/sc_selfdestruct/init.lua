AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:SpawnFunction(ply, tr, classname)
	if (!tr.Hit) then return end

	local ent = ents.Create(classname)
	ent:SetPos(tr.HitPos + tr.HitNormal * 16)
	ent:Spawn()
	ent.SC_Owner = ply
	ent:Activate()

	return ent
end

function ENT:Initialize()
	self.Entity:SetModel("models/Combine_Helicopter/helicopter_bomb01.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)

	local phys = self:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
	end

	self.Inputs = Wire_CreateInputs(self.Entity, { "Destruct"})	
	self:SetOverlayText(self.PrintName)
end

function ENT:OnRemove()
	Wire_Remove(self)
end

function ENT:Use(ply)

end

function ENT:Think()
	self.BaseClass.Think(self)
end

function ENT:Destruct()
	if !IsValid(self.SC_CoreEnt) then return false end
	if self.SC_CoreEnt.SC_Owner != self.SC_Owner then return false end

	local dmginfo = DamageInfo()
	dmginfo:SetAttacker(self)
	dmginfo:SetInflictor(self.SC_CoreEnt)
	SC_KillEnt(self.SC_CoreEnt, dmginfo)
end

function ENT:TriggerInput(iname, value)
	if (iname == "Destruct") then
		if(value >= 1) then
			self:Destruct()
		end
	end
end