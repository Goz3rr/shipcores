ENT.Type			= "anim"
ENT.Base			= "base_wire_entity"
ENT.PrintName		= "Self Destruct"
ENT.Information		= "Destroys a cored contraption instantly"
ENT.Author			= "Goz3rr"
ENT.Category		= "Space Combat"

ENT.Spawnable		= true
ENT.AdminSpawnable	= false