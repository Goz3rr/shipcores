include('shared.lua')

// Leave this, it prevents the default info bubble
function ENT:Think()
end

// Draw the core and info bubble
function ENT:DoNormalDraw()
	local e = self.Entity
	if (LocalPlayer():GetEyeTrace().Entity == e and EyePos():Distance(e:GetPos()) < 1024) then
		if ( self.RenderGroup == RENDERGROUP_OPAQUE) then
			self.OldRenderGroup = self.RenderGroup
			self.RenderGroup = RENDERGROUP_TRANSLUCENT
		end
		e:DrawModel()
		if(self:GetOverlayText() ~= "") then
			AddWorldTip(e:EntIndex(),self:GetOverlayText(),0.5,e:GetPos(),e)
		end
	else
		if(self.OldRenderGroup) then
			self.RenderGroup = self.OldRenderGroup
			self.OldRenderGroup = nil
		end
		e:DrawModel()
	end	
end

// Leave this, it prevents the default info bubble
function ENT:DrawEntityOutline(size)
end

// Used to round numbers in the GUI
function sc_round(number)
	return string.format("%G", number)
end

// Open the GUI
net.Receive("sc_open_core", function(len)
	local ent = net.ReadEntity()
	local linkedProps = net.ReadTable()
	local ShipName = net.ReadString()
	local HullHP, HullMax = net.ReadFloat(), net.ReadFloat()
	local ArmorHP, ArmorMax = net.ReadFloat(), net.ReadFloat()
	local ShieldHP, ShieldMax = net.ReadFloat(), net.ReadFloat()
	local CPU, CPUMax = net.ReadFloat(), net.ReadFloat()
	local PG, PGMax = net.ReadFloat(), net.ReadFloat()
	local Air, Energy, Coolant = net.ReadFloat(), net.ReadFloat(), net.ReadFloat()
	
	local newCPU = CPU // update this
	local newShield, newArmour, newHull = 0, 0, 0 // and this

	--[[
	local TestFrame = vgui.Create("DFrame")
	TestFrame:SetPos(300,100)
	TestFrame:SetSize(200,200)
	TestFrame:SetTitle("testing testing")
	TestFrame:MakePopup()

	local test = vgui.Create("DVerticalSlider", TestFrame)
	test:SetSize(100, 100)
	test:SetPos(10, 50)
	]]

	local Frame = vgui.Create("DFrame")
	Frame:SetPos(100,100)
	Frame:SetSize(450,550)
	Frame:SetTitle("Ship Info - " .. ShipName)
	Frame:MakePopup()
	//Frame:Close()

	local PropertySheet = vgui.Create("DPropertySheet", Frame)
	PropertySheet:SetPos(5, 30)
	PropertySheet:SetSize(440, 515)

	// Overview Tab
	local OverviewPage = vgui.Create("DPanel", Frame)
	OverviewPage:SetPaintBackground(false)

	local ShipNameTxt = vgui.Create("DTextEntry", OverviewPage)
	ShipNameTxt:Dock(TOP)
	ShipNameTxt:SetValue(ShipName)
	ShipNameTxt:SetEnterAllowed(true)

	ShipNameTxt.OnEnter = function(text)
		ShipName = text:GetValue()
		net.Start("sc_set_name")
			net.WriteEntity(ent)
			net.WriteString(ShipName)
		net.SendToServer()
		Frame:SetTitle("Ship Info - " .. ShipName)
	end

	local icon = vgui.Create("DMultiModelPanel", OverviewPage)
	icon:Dock(FILL)

	local centerOfMass = Vector(0,0,0)
	local propCount = 1
	local highestPos = 0
	local entCenter = ent:GetPos()
	local phys = ent:GetPhysicsObject()
	if phys and phys:IsValid() then
		entCenter = phys:GetMassCenter()
	end
	for _,prop in pairs(linkedProps) do
		if IsValid(prop) and prop != ent then
			local propCenter = prop:GetPos()
			local propPhys = prop:GetPhysicsObject()
			if propPhys and propPhys:IsValid() then
				propCenter = propPhys:GetMassCenter()
			end
			local relative = propCenter - entCenter
			centerOfMass = centerOfMass + relative
			propCount = propCount + 1

			if relative.X - prop:OBBMins().X > highestPos then highestPos = relative.X - prop:OBBMins().X end
			if relative.Y - prop:OBBMins().Y > highestPos then highestPos = relative.Y - prop:OBBMins().Y end
			if relative.Z - prop:OBBMins().Z > highestPos then highestPos = relative.Z - prop:OBBMins().Z end
			if relative.X + prop:OBBMaxs().X > highestPos then highestPos = relative.X + prop:OBBMaxs().X end
			if relative.Y + prop:OBBMaxs().Y > highestPos then highestPos = relative.Y + prop:OBBMaxs().Y end
			if relative.Z + prop:OBBMaxs().Z > highestPos then highestPos = relative.Z + prop:OBBMaxs().Z end
		end
	end
	centerOfMass = centerOfMass / propCount

	icon:SetCenter(centerOfMass)
	icon:SetZoom(highestPos*1.5 + 100)

	local shipModels = {}
	table.insert(shipModels, { Model=ent:GetModel() })
	for _,prop in pairs(linkedProps) do
		if IsValid(prop) and prop != ent then
			local relativePos = prop:GetPos() - ent:GetPos()
			local relativeAng = prop:GetAngles() - ent:GetAngles()
			table.insert(shipModels, { Model=prop:GetModel(), Pos=relativePos, Ang=relativeAng, Mat=prop:GetMaterial(), Col=prop:GetColor() })
		end
	end
	icon:SetModels(unpack(shipModels))
	// End of overview tab

	// LS Tab
	local LSPage = vgui.Create("DPanel", Frame)
	LSPage:SetPaintBackground(false)

	local LSCol = vgui.Create("DListView", LSPage)
	LSCol:Dock(TOP)
	LSCol:SetTall(68)
	LSCol:Clear()
	LSCol:SetMultiSelect(false)
	LSCol:AddColumn("Type")
	LSCol:AddColumn("Amount")

	LSCol:AddLine("Air", Air)
	LSCol:AddLine("Energy", Energy)
	LSCol:AddLine("Coolant", Coolant)
	// End of LS Tab

	// Health Tab
	local HealthPage = vgui.Create("DPanel", Frame)
	HealthPage:SetPaintBackground(false)

	local HPCol = vgui.Create("DListView", HealthPage)
	HPCol:Dock(FILL)
	HPCol:Clear()
	HPCol:SetMultiSelect(false)
	HPCol:AddColumn("Type")
	HPCol:AddColumn("Amount")
	HPCol:AddColumn("Max Amount")
	HPCol:AddColumn("Percent")

	HPCol:AddLine("Hull", sc_round(HullHP), sc_round(HullMax), sc_round(HullHP/HullMax * 100))
	HPCol:AddLine("Armor", sc_round(ArmorHP), sc_round(ArmorMax), sc_round(ArmorHP/ArmorMax * 100))
	HPCol:AddLine("Shield", sc_round(ShieldHP), sc_round(ShieldMax), sc_round(ShieldHP/ShieldMax * 100))
	HPCol:AddLine("", "", "", "")
	HPCol:AddLine("CPU", sc_round(CPU), sc_round(CPUMax), sc_round(CPU/CPUMax * 100))
	HPCol:AddLine("PowerGrid", sc_round(PG), sc_round(PGMax), sc_round(PG/PGMax * 100))

	// End of health tab

	// Upgrades Tab

	local UpgradesPage = vgui.Create("DPanelList", Frame)
	UpgradesPage:SetPaintBackground(false)
	UpgradesPage:SetAutoSize(true)
	UpgradesPage:SetSpacing(5)
	UpgradesPage:EnableHorizontal(false)

	local CPUPointList = vgui.Create("DListView", UpgradesPage)
	CPUPointList:SetMultiSelect(false)
	CPUPointList:AddColumn("CPU Points")
	CPUPointList:AddLine(CPU.."/"..CPUMax)
	CPUPointList:SizeToContents()

	local ShieldSlider = vgui.Create("DNumSlider", UpgradesPage)
	ShieldSlider:SetText("Shield Points")
	ShieldSlider:SetMin(0)
	ShieldSlider:SetValue(0)
	ShieldSlider:SetMax(CPUMax)
	ShieldSlider:SetDecimals(0)

	local ArmourSlider = vgui.Create("DNumSlider", UpgradesPage)
	ArmourSlider:SetText("Armor Points")
	ArmourSlider:SetMin(0)
	ArmourSlider:SetValue(0)
	ArmourSlider:SetMax(CPUMax)
	ArmourSlider:SetDecimals(0)
	
	local CapSlider = vgui.Create("DNumSlider", UpgradesPage)
	CapSlider:SetText("Capacitor Points")
	CapSlider:SetMin(0)
	CapSlider:SetValue(0)
	CapSlider:SetMax(CPUMax)
	CapSlider:SetDecimals(0)

	ShieldSlider.OnValueChanged = function(panel, value)
		newShield = ShieldSlider:GetValue()
		newCPU = ShieldSlider:GetValue() + ArmourSlider:GetValue() + CapSlider:GetValue()
		ArmourSlider:SetMax(CPUMax-value-CapSlider:GetValue())
		ArmourSlider.Slider:SetSlideX(ArmourSlider.Scratch:GetFraction(ArmourSlider:GetValue()))
		CapSlider:SetMax(CPUMax-value-ArmourSlider:GetValue())
		CapSlider.Slider:SetSlideX(CapSlider.Scratch:GetFraction(CapSlider:GetValue()))
	end

	ArmourSlider.OnValueChanged = function(panel, value)
		newArmour = ArmourSlider:GetValue()
		newCPU = ShieldSlider:GetValue() + ArmourSlider:GetValue() + CapSlider:GetValue()
		ShieldSlider:SetMax(CPUMax-value-CapSlider:GetValue())
		ShieldSlider.Slider:SetSlideX(ShieldSlider.Scratch:GetFraction(ShieldSlider:GetValue()))
		CapSlider:SetMax(CPUMax-value-ShieldSlider:GetValue())
		CapSlider.Slider:SetSlideX(CapSlider.Scratch:GetFraction(CapSlider:GetValue()))
	end

	CapSlider.OnValueChanged = function(panel, value)
		newCap = CapSlider:GetValue()
		newCPU = ShieldSlider:GetValue() + ArmourSlider:GetValue() + CapSlider:GetValue()
		ShieldSlider:SetMax(CPUMax-value-ArmourSlider:GetValue())
		ShieldSlider.Slider:SetSlideX(ShieldSlider.Scratch:GetFraction(ShieldSlider:GetValue()))
		ArmourSlider:SetMax(CPUMax-value-ShieldSlider:GetValue())
		ArmourSlider.Slider:SetSlideX(ArmourSlider.Scratch:GetFraction(ArmourSlider:GetValue()))
	end

	local PGPointList = vgui.Create("DListView", UpgradesPage)
	PGPointList:SetMultiSelect(false)
	PGPointList:AddColumn("Power Grid Points")
	PGPointList:AddLine(PG.."/"..PGMax)
	PGPointList:SizeToContents()

	local ResSelect = vgui.Create("DButton", UpgradesPage)
	ResSelect:SetText("Select Resistance Target")
	ResSelect.DoClick = function(btn)
		local ResSelectOptions = DermaMenu()
		ResSelectOptions:AddOption("Shield Resistance", function() 	ResSelect:SetText("Shield Resistance") end)
		ResSelectOptions:AddOption("Armor Resistance", function() 	ResSelect:SetText("Armor Resistance") end)
		ResSelectOptions:Open()
	end
	
	local KineticSlider = vgui.Create("DNumSlider", UpgradesPage)
	KineticSlider:SetText("Kinetic Resistance")
	KineticSlider:SetMin(0)
	KineticSlider:SetMax(255)
	KineticSlider:SetValue(0)

	local ExplosiveSlider = vgui.Create("DNumSlider", UpgradesPage)
	ExplosiveSlider:SetText("Explosive Resistance")
	ExplosiveSlider:SetMin(0)
	ExplosiveSlider:SetMax(255)
	ExplosiveSlider:SetValue(0)
	
	local EmSlider = vgui.Create("DNumSlider", UpgradesPage)
	EmSlider:SetText("EM Resistance")
	EmSlider:SetMin(0)
	EmSlider:SetMax(255)
	EmSlider:SetValue(0)
	
	local ThermalSlider = vgui.Create("DNumSlider", UpgradesPage)
	ThermalSlider:SetText("Thermal Resistance")
	ThermalSlider:SetMin(0)
	ThermalSlider:SetMax(255)
	ThermalSlider:SetValue(0)

	UpgradesPage:AddItem(CPUPointList)
	UpgradesPage:AddItem(ShieldSlider)
	UpgradesPage:AddItem(ArmourSlider)
	UpgradesPage:AddItem(CapSlider)

	UpgradesPage:AddItem(PGPointList)
	UpgradesPage:AddItem(ResSelect)
	UpgradesPage:AddItem(KineticSlider)
	UpgradesPage:AddItem(ExplosiveSlider)
	UpgradesPage:AddItem(EmSlider)
	UpgradesPage:AddItem(ThermalSlider)
	// End of upgrades tab

	PropertySheet:AddSheet("Overview", OverviewPage, "icon16/world.png", false, false, "Ship Overview")
	PropertySheet:AddSheet("Life Support", LSPage, "icon16/heart.png", false, false, "Life support status")
	PropertySheet:AddSheet("Health", HealthPage, "icon16/shield.png", false, false, "Current ship health")
	PropertySheet:AddSheet("Upgrades", UpgradesPage, "icon16/wrench.png", false, false, "Adjust core attributes")
end)