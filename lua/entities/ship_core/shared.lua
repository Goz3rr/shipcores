ENT.Type			= "anim"
ENT.Base			= "base_wire_entity"
ENT.PrintName		= "Ship Core"
ENT.Information		= "Ship Core"
ENT.Author			= "Goz3rr"
ENT.Category		= "Space Combat"

ENT.Spawnable		= true
ENT.AdminSpawnable	= false

ENT.DefaultModel	= "models/Combine_Helicopter/helicopter_bomb01.mdl"
ENT.RecalcTime		= 3
ENT.SC_Name 		= "Ship Core"
ENT.SC_Size			= 2

ENT.HitSounds = {
	"space_combat/shield_impact_01.wav",
	"space_combat/shield_impact_02.wav",
	"space_combat/shield_impact_03.wav",
	"space_combat/shield_impact_04.wav"	
}

for _,s in pairs(ENT.HitSounds) do // Load all sounds
	resource.AddFile("sound/" .. s)
	util.PrecacheSound(s)
end

ENT.BaseFitting = {
	CPU		= 50,
	CPU_Max	= 50,
	CPU_R	= 1.00,
	PG		= 50,
	PG_Max	= 50,
	PG_R	= 1.00
}

ENT.BaseCap = {
	Max = 175,
	CAP = 175,
	Ratio = 0.25,
	RechargeRate = 20
}

ENT.BaseHull = {
	Max = 50,
	HP = 50,
	Ratio = 1.00,
	RepairRate = 0
}

ENT.BaseArmor = {
	Max = 200,
	HP = 200,
	Ratio = 1.10,
	RepairRate = 0
}

ENT.BaseShield = {
	Max = 250,
	HP = 250,
	Ratio = 1.25,
	RepairRate = 10
}

ENT.BaseHullRes = {
	EM		= 0.00,
	EXP		= 0.00,
	KIN		= 0.00,
	THERM	= 0.00
}

ENT.BaseArmorRes = {
	EM		= 0.60,
	EXP		= 0.10,
	KIN		= 0.20,
	THERM	= 0.40
}

ENT.BaseShieldRes = {
	EM		= 0.00,
	EXP		= 0.60,
	KIN		= 0.40,
	THERM	= 0.20
}