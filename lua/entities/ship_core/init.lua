AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

util.AddNetworkString("sc_open_core")

function ENT:SpawnFunction(ply, tr, classname)
	if (!tr.Hit) then return end

	local ent = ents.Create(classname)
	ent:SetPos(tr.HitPos + tr.HitNormal * 16)
	ent:Spawn()
	ent.SC_Owner = ply
	ent:Activate()

	return ent
end

function ENT:Initialize()
	if(self:GetModel() == "models/error.mdl") then
		self:SetModel(self.DefaultModel) // Set the model if we don't have one
	end
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)

	local phys = self:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
	end

	self.NextRecalc = CurTime() + self.RecalcTime // Next health calculation
	self.NextRepair = CurTime() + 1 // Next repair calculation
	self.SC_CoreEnt = self

	if WireAddon then // Add wire outputs to the core
		self.Outputs = Wire_CreateOutputs(self, { 
			"Shield", 
			"Armor", 
			"Hull",
			"Capacitor", 
			"Max Shield", 
			"Max Armor", 
			"Max Hull",
			"Max Capacitor",
			"% Shield",
			"% Armor",
			"% Hull",
			"% Capacitor"
		})
	end

	if RES_DISTRIB == 2 then
		RD_AddResource(self, "air", 0)
		RD_AddResource(self, "energy", 0)
		RD_AddResource(self, "coolant", 0)
		LS_RegisterEnt(self, "Climate")
	end

	// Set core stats to the default
	self.Hull = table.Copy(self.BaseHull)
	self.HullRes = table.Copy(self.BaseHullRes)
	self.Armor = table.Copy(self.BaseArmor)
	self.ArmorRes = table.Copy(self.BaseArmorRes)
	self.Shield = table.Copy(self.BaseShield)
	self.ShieldRes = table.Copy(self.BaseShieldRes)

	self.Cap = table.Copy(self.BaseCap)
	self.Slots = table.Copy(self.BaseSlots)
	self.Fitting = table.Copy(self.BaseFitting)

	// Force a health calculation
	self:CalcHealth()
end

// Calculate core stats
function ENT:CalcHealth()
	local start = SysTime() // Start of the calculation

	local percent = self.Hull.HP / self.Hull.Max
	if self.LinkedProps then // Reset the cores on all linked props because they may have been unwelded
		for _,ent in pairs(self.LinkedProps) do
			if IsValid(ent) and ent != self.Entity then
				ent.SC_CoreEnt = nil
				if ent.SC_Health then
					SC_SetHealth(ent, ent.SC_Health * percent)
				end
			end
		end
	end

	self.LinkedProps = SC_GetLinkedProps(self.Entity)
	if !self.LinkedProps then return end

	local NewHull = 5
	local NewHullRatio = 1

	for _,ent in pairs(self.LinkedProps) do
		if IsValid(ent) then
			if ent != self.Entity and IsValid(ent.SC_CoreEnt) and ent.SC_CoreEnt != self.Entity and ent.SC_CoreEnt:GetClass() == "ship_core" then
				// This prop already has a valid core, we don't want anything to do with it
				constraint.RemoveAll(self.Entity)
				return
			else
				ent.SC_CoreEnt = self.Entity

				if not string.find(ent:GetClass(), "wire") then
					local volume = SC_GetVolume(ent)
					local multiplier = SC_GetHealthMultiplier(ent)
					local health = math.Round(volume^1.000*0.75/10/17500)*multiplier*10

					if ent.SC_Health and ent.SC_Health < ent.SC_MaxHealth then
						NewHullRatio = (NewHullRatio + ent.SC_Health / ent.SC_MaxHealth) / 2
						SC_SetHealth(ent, ent.SC_MaxHealth)
					else
						NewHullRatio = (NewHullRatio + 1) / 2
					end

					NewHull = NewHull + health
				end
			end
		end
	end

	self.Hull.Percent = self.Hull.HP / self.Hull.Max
	self.Hull.Max = math.Round(NewHull * self.Hull.Ratio/10)*10
	if self.Hull.Max < self.BaseHull.HP then self.Hull.Max = self.BaseHull.HP end
	self.Hull.HP = self.Hull.Max * NewHullRatio * self.Hull.Percent

	self.Armor.Percent = self.Armor.HP / self.Armor.Max
	self.Armor.Max = math.Round(NewHull * self.Armor.Ratio/10)*10
	if self.Armor.Max < self.BaseArmor.HP then self.Armor.Max = self.BaseArmor.HP end
	self.Armor.HP = self.Armor.Max * NewHullRatio * self.Armor.Percent
	
	self.Shield.Percent = self.Shield.HP / self.Shield.Max
	self.Shield.Max = math.Round(NewHull * self.Shield.Ratio/10)*10
	if self.Shield.Max < self.BaseShield.HP then self.Shield.Max = self.BaseShield.HP end
	self.Shield.HP = self.Shield.Max * NewHullRatio * self.Shield.Percent


	self.Cap.Percent = self.Cap.CAP / self.Cap.Max
	self.Cap.Max = math.Round(NewHull * self.Cap.Ratio^1.175)
	if self.Cap.Max < self.BaseCap.CAP then self.Cap.Max = self.BaseCap.CAP end
	self.Cap.CAP = self.Cap.Max * self.Cap.Percent
	self.Cap.RechargeRate = math.floor((100 + ((NewHull/2.6)^0.7125))/1.25) 

	self.Fitting.CPU_Max = self.Shield.Max + self.Armor.Max + (self.Cap.Max*3)//math.floor(100+(((NewHull/37.5)^0.815)*self.Fitting.CPU_R))
	self.Fitting.CPU = self.Fitting.CPU_Max
	self.Fitting.PG_Max = math.floor(25+(((((NewHull^1.50)/50)/750)^1.6275)*self.Fitting.PG_R))
	self.Fitting.PG = self.Fitting.PG_Max
end

// Apply damage to the core, damage is a table with the EM, EXP, KIN and THERM damages
function ENT:ApplyDamage(ent, damage, dmginfo)
	local data = EffectData()

	local newHP, damageRemain = SC_TotalDamage(self.Shield.HP, damage, self.ShieldRes)
	if newHP < 0 then
		self.Shield.HP = 0
		local newHP, damageRemain = SC_TotalDamage(self.Armor.HP, SC_AddDamages(damage, damageRemain) , self.ArmorRes)
		if newHP < 0 then
			self.Armor.HP = 0
			local newHP, damageRemain = SC_TotalDamage(self.Hull.HP, SC_AddDamages(damage, damageRemain) , self.HullRes)
			if newHP < 0 then
				sc_debug("Core " .. self:EntIndex() .. " got destroyed")
				SC_KillEnt(self, dmginfo)
				return
			else
				sc_debug("Core " .. self:EntIndex() .. " took " .. self.Hull.HP - newHP .. " damage to hull")
				self.Hull.HP = newHP
				data:SetColor(1)
			end
		else
			sc_debug("Core " .. self:EntIndex() .. " took " .. self.Armor.HP - newHP .. " damage to armor")
			self.Armor.HP = newHP
			data:SetColor(2)
		end
	else
		sc_debug("Core " .. self:EntIndex() .. " took " .. self.Shield.HP - newHP .. " damage to shield")
		self.Shield.HP = newHP
		data:SetColor(7)
	end

	if !ent.SC_Effect then
		data:SetEntity(ent)
		util.Effect("sc_core_hit", data, true, true)
		ent:EmitSound(self.HitSounds[math.random(1,4)], 100, math.Rand(90,110))
		ent.SC_Effect = true
		timer.Simple(0.8, function() ent.SC_Effect = false end)
	end

	return false
end

// Called when the core is removed
function ENT:OnRemove()
	if self.LinkedProps then
		local mul = self.Hull.HP / self.Hull.Max
		for _,i in pairs(self.LinkedProps) do
			if IsValid(i) and i.SC_Health and i.SC_MaxHealth then
				i.SC_CoreEnt = nil
				i.SC_Health = i.SC_MaxHealth * mul
			end
		end
	end

	if SB_Remove_Environment and self.Env then
		SB_Remove_Environment(self.Env)
	end

	Wire_Remove(self)
end

// Open the core GUI
function ENT:Use(activator, caller, usetype, value)
	net.Start("sc_open_core") // All these are used for the GUI stats
		net.WriteEntity(self)
		net.WriteTable(self.LinkedProps)
		net.WriteString(self.SC_Name)
		net.WriteFloat(self.Hull.HP)
		net.WriteFloat(self.Hull.Max)
		net.WriteFloat(self.Armor.HP)
		net.WriteFloat(self.Armor.Max)
		net.WriteFloat(self.Shield.HP)
		net.WriteFloat(self.Shield.Max)
		net.WriteFloat(self.Fitting.CPU)
		net.WriteFloat(self.Fitting.CPU_Max)
		net.WriteFloat(self.Fitting.PG)
		net.WriteFloat(self.Fitting.PG_Max)
		net.WriteFloat(RD_GetResourceAmount(self, "air"))
		net.WriteFloat(RD_GetResourceAmount(self, "energy"))
		net.WriteFloat(RD_GetResourceAmount(self, "coolant"))
	net.Send(activator)
end

// Core update
function ENT:Think()
	// Set the info bubble
	self:SetOverlayText(self.SC_Name .. "\n\n"
	.. string.format("Hull: %G/%G (%G)", self.Hull.HP, self.Hull.Max, self.Hull.RepairRate) .. "\n"
	.. string.format("Armor: %G/%G (%G)", self.Armor.HP, self.Armor.Max, self.Armor.RepairRate) .. "\n"
	.. string.format("Shield: %G/%G (%G)", self.Shield.HP, self.Shield.Max, self.Shield.RepairRate) .. "\n"
	.. "\n"
	.. string.format("Capacitor: %G/%G (%G)", self.Cap.CAP, self.Cap.Max, self.Cap.RechargeRate) .. "\n"
	.. string.format("CPU: %G/%G", self.Fitting.CPU, self.Fitting.CPU_Max) .. "\n"
	.. string.format("PG: %G/%G", self.Fitting.PG, self.Fitting.PG_Max))

	// Check if we need to calculate health
	if (self.NextRecalc <= CurTime()) then
		self.NextRecalc = CurTime() + self.RecalcTime
		self:CalcHealth()
	end

	// Check if we need to check regeneration
	if (self.NextRepair <= CurTime()) then
		self.NextRepair= CurTime() + 1
		if self.Shield.HP < self.Shield.Max and self.Shield.RepairRate > 0 then
			local C_max = self.Shield.Max
			local C = math.Clamp(self.Shield.HP, 1, self.Shield.Max)

			local optimal = C_max/2
			local x = (C / optimal - 1)
			local R = (1/math.exp(x^2)) * self.Shield.RepairRate
			local HP = C + R

			sc_debug("Shield regenerated " .. (HP-self.Shield.HP) .. " at regen rate " .. R)

			if HP < C_max then
				self.Shield.HP = HP
			else
				self.Shield.HP = C_max
			end
		end

		if self.Cap.CAP < self.Cap.Max and self.Cap.RechargeRate > 0 then
			local C_max		= self.Cap.Max
			local C 		= math.Clamp(self.Cap.CAP, 1, self.Cap.Max)
			local R_time	= self.Cap.RechargeRate	    
			local tau 		= 5.0/R_time
			local formula	= tau*(1-C/C_max)*math.sqrt(2*C/C_max - (C/C_max)^2)
			local HP		= (math.Clamp(formula * C_max, 0.1 ,self.Cap.Max)) + C

			if HP < self.Cap.Max then
				self.Cap.CAP = HP
			else
				self.Cap.CAP = self.Cap.Max
			end
		end
	end

	self:Displays()
end

// Update the wire outputs
function ENT:Displays()
	Wire_TriggerOutput(self.Entity, "Shield", self.Shield.HP)
	Wire_TriggerOutput(self.Entity, "Armor", self.Armor.HP)
	Wire_TriggerOutput(self.Entity, "Hull", self.Hull.HP)
	Wire_TriggerOutput(self.Entity, "Capacitor", self.Cap.CAP)
	
	Wire_TriggerOutput(self.Entity, "Max Shield", self.Shield.Max)
	Wire_TriggerOutput(self.Entity, "Max Armor", self.Armor.Max)
	Wire_TriggerOutput(self.Entity, "Max Hull", self.Hull.Max)
	Wire_TriggerOutput(self.Entity, "Max Capacitor", self.Cap.Max)
	
	Wire_TriggerOutput(self.Entity, "% Shield", self.Shield.HP/self.Shield.Max*100)
	Wire_TriggerOutput(self.Entity, "% Armor", self.Armor.HP/self.Armor.Max*100)
	Wire_TriggerOutput(self.Entity, "% Hull", self.Armor.HP/self.Armor.Max*100)
	Wire_TriggerOutput(self.Entity, "% Capacitor", self.Cap.CAP/self.Cap.Max*100)
end

// Leave this, it prevents the default info bubble
function ENT:Draw()

end