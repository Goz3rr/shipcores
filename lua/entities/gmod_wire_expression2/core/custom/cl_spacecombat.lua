E2Helper.Descriptions["getCore"] = "Returns the ship core of this entity"

E2Helper.Descriptions["coreShieldHP"] = "Gets the current shield"
E2Helper.Descriptions["coreShieldMax"] = "Gets the maximum amount of shield"
E2Helper.Descriptions["coreArmorHP"] = "Gets the current armor"
E2Helper.Descriptions["coreArmorMax"] = "Gets the maximum amount of armor"
E2Helper.Descriptions["coreHullHP"] = "Gets the current hull"
E2Helper.Descriptions["coreHullMax"] = "Gets the maximum amount of hull"
E2Helper.Descriptions["coreCap"] = "Gets the current capacitor"
E2Helper.Descriptions["coreCapMax"] = "Gets the maximum amount of capacitor"

E2Helper.Descriptions["shootBeamTo"] = "Shoot beam"
E2Helper.Descriptions["scExplode"] = "Create an explosion"