e2function entity entity:getCore()
	if !IsValid(this) then return end
	return this.SC_CoreEnt
end

e2function number entity:coreShieldHP()
	if !IsValid(this) or !IsValid(this.SC_CoreEnt) then return nil end
	return this.SC_CoreEnt.Shield.HP
end

e2function number entity:coreShieldMax()
	if !IsValid(this) or !IsValid(this.SC_CoreEnt) then return nil end
	return this.SC_CoreEnt.Shield.Max
end

e2function number entity:coreArmorHP()
	if !IsValid(this) or !IsValid(this.SC_CoreEnt) then return nil end
	return this.SC_CoreEnt.Armor.HP
end

e2function number entity:coreArmorMax()
	if !IsValid(this) or !IsValid(this.SC_CoreEnt) then return nil end
	return this.SC_CoreEnt.Armor.Max
end

e2function number entity:coreHullHP()
	if !IsValid(this) or !IsValid(this.SC_CoreEnt) then return nil end
	return this.SC_CoreEnt.Hull.HP
end

e2function number entity:coreHullMax()
	if !IsValid(this) or !IsValid(this.SC_CoreEnt) then return nil end
	return this.SC_CoreEnt.Hull.Max
end

e2function number entity:coreCap()
	if !IsValid(this) or !IsValid(this.SC_CoreEnt) then return nil end
	return this.SC_CoreEnt.Cap.CAP
end

e2function number entity:coreCapMax()
	if !IsValid(this) or !IsValid(this.SC_CoreEnt) then return nil end
	return this.SC_CoreEnt.Cap.Max
end

e2function void entity:selfDestruct()
	if !IsValid(this) or !IsValid(this.SC_CoreEnt) then return end
	if this.SC_CoreEnt.SC_Owner != self.player then return end

	local dmginfo = DamageInfo()
	dmginfo:SetAttacker(self.entity)
	dmginfo:SetInflictor(this.SC_CoreEnt)
	SC_KillEnt(this.SC_CoreEnt, dmginfo)
end

function shootBeam(owner, ent, powersource, direction, emp, expl, kinetic, thermal)
	if !IsValid(ent) or !IsValid(powersource.SC_CoreEnt) then return end
	if powersource.SC_CoreEnt.SC_Owner != owner then return end

	sc_debug("Shooting beam from entity " .. ent:EntIndex() .. " with owner " .. owner:Nick())

	local pos = ent:GetPos()
	local trace = {}
	trace.start = pos
	trace.endpos = pos + Vector(direction[1]*50000, direction[2]*50000, direction[3]*50000)
	trace.filter = ent 
	local tr = util.TraceLine(trace)

	if tr.Hit then
		local cap = ((emp+expl+kinetic+thermal)/10)^1.05
		if powersource.SC_CoreEnt.Cap.CAP >= cap then
			sc_debug("Shot costs " .. cap)
			powersource.SC_CoreEnt.Cap.CAP = powersource.SC_CoreEnt.Cap.CAP - cap
			if IsValid(tr.Entity) then
				local dmginfo = DamageInfo()
				dmginfo:SetAttacker(owner)
				dmginfo:SetInflictor(ent)
				SC_ApplyDamage(tr.Entity, SC_DamageTable(emp, expl, kinetic, thermal), dmginfo)
			end
		end
	else
		sc_debug("SBT Trace failed")
	end
end

function SC_Explode(owner, powersource, radius, emp, expl, kinetic, thermal)
	if !IsValid(powersource) or !IsValid(powersource.SC_CoreEnt) then return end
end

e2function void entity:shootBeamTo(vector Direction, number emp, number expl, number kinetic, number thermal)
	return shootBeam(self.player, this, this, Direction, emp, expl, kinetic, thermal)
end

e2function void entity:shootBeamTo(entity powersource, vector Direction, number emp, number expl, number kinetic, number thermal)
	return shootBeam(self.player, this, powersource, Direction, emp, expl, kinetic, thermal)
end

e2function void entity:scExplode(number radius, number emp, number expl, number kinetic, number thermal)
	return SC_Explode(self.player, this, radius, emp, expl, kinetic, thermal)
end

e2function void entity:scExplode(entity powersource, number radius, number emp, number expl, number kinetic, number thermal)
	return SC_Explode(self.player, powersource, radius, emp, expl, kinetic, thermal)
end