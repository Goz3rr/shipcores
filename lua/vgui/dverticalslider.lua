local PANEL = {}

function PANEL:Init()
	self.Slider = self:Add("DSlider", self)
	self.Slider:SetLockY(0.5)
	self.Slider:SetTrapInside(true)
	self.Slider:SetHeight(16)
	Derma_Hook(self.Slider, "Paint", "Paint", "NumSlider")
end

derma.DefineControl("DVerticalSlider", "Vertical slider", PANEL, "Panel")