local PANEL = {}

function PANEL:Init()
	self.Models = {}

	self:SetCamPos(Vector(100, 50, 50))
	self:SetLookAt(Vector(0, 0, 0))
	self:SetFOV(70)

	self.Center = Vector(0, 0, 0)
	self.Zoom = 100
end

function PANEL:SetCenter(pos)
	self:SetLookAt(pos)
	self.Center = pos
end

function PANEL:SetZoom(zoom)
	self.Zoom = zoom
end

function PANEL:SetModels(...)
	if (!ClientsideModel) then return end

	self.Models = {}
	for _,mdl in pairs({...}) do
		local cmdl = ClientsideModel(mdl.Model or "models/error.mdl", RENDER_GROUP_OPAQUE_ENTITY)
		if IsValid(cmdl) then
			cmdl:SetNoDraw(true)
			cmdl:SetColor(mdl.Col)
			cmdl:SetMaterial(mdl.Mat)
			table.insert(self.Models, { Model=cmdl, Pos=mdl.Pos or Vector(0,0,0), Ang=mdl.Ang or Angle(0,0,0) })
		end
	end
end

function PANEL:Paint()
	if !self.Models or #self.Models == 0 then return end
	local x, y = self:LocalToScreen(0, 0)

	local ang = self.aLookAngle
	if (!ang) then
		ang = (self.vLookatPos-self.vCamPos):Angle()
	end

	self.vCamPos = Vector(math.cos(math.rad(RealTime()*10)) * self.Zoom, math.sin(math.rad(RealTime()*10)) * self.Zoom, 50) + self.Center

	local w, h = self:GetSize()
	cam.Start3D(self.vCamPos, ang, self.fFOV, x, y, w, h, 5, 4096)
		for _,mdl in pairs(self.Models) do
			mdl.Model:SetAngles(mdl.Ang)
			mdl.Model:SetPos(mdl.Pos)
			mdl.Model:DrawModel()
		end
	cam.End3D()
end

derma.DefineControl("DMultiModelPanel", "A panel containing multiple models", PANEL, "DModelPanel")