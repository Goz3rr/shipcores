SpaceCombat = 1
SpaceCombat_version = "GM13 0.2"
SpaceCombat_debug = true // print debug messages
SC_ModelBlacklist = { // Blacklisted models
	"models/props_combine/combine_citadel001.mdl"
}

print("\n===========================================")
print("== Space Combat Version: " .. SpaceCombat_version .. " loaded ==")
print("===========================================\n")

util.AddNetworkString("sc_set_name")

CreateConVar("scvar_physdmg", 1) // Take damage from collisions/physics
CreateConVar("scvar_propdmg", 1) // Non cored props can be destroyed

resource.AddFile("materials/space_combat/shield.vmt")
resource.AddFile("materials/space_combat/shield.vtf")
AddCSLuaFile("vgui/dmultimodelpanel.lua")

local SC_MaxPropHealth = 1000000 // Max health for a non cored prop
local SC_MinPropHealth = 100 // Min health for a non cored prop

local MatMultiplier = { // Material multipliers
	[MAT_ANTLION] = 0.50,
	[MAT_BLOODYFLESH] = 0.25,
	[MAT_CONCRETE] = 1.50,
	[MAT_DIRT] = 0.20,
	[MAT_EGGSHELL] = 0.05,
	[MAT_FLESH] = 0.25,
	[MAT_GRATE] = 1.50,
	[MAT_ALIENFLESH] = 0.50,
	[MAT_CLIP] = 0.01,
	[MAT_PLASTIC] = 0.75,
	[MAT_METAL] = 2.00,
	[MAT_SAND] = 0.15,
	[MAT_FOLIAGE] = 0.10,
	[MAT_COMPUTER] = 1.00,
	[MAT_SLOSH] = 0.10,
	[MAT_TILE] = 0.50,
	[MAT_VENT] = 1.50,
	[MAT_WOOD] = 0.25,
	[MAT_GLASS] = 0.50,
	[MAT_WARPSHIELD] = 1.00
}

duplicator.RegisterEntityClass("ship_core", function(ply, data)
	local core = ents.Create("ship_core")
	core:SetPos(data.Pos)
	core:SetAngles(data.Angle)
	core:SetModel(data.Model)
	core:Spawn()
	core.SC_Owner = ply
	core:Activate()

	sc_debug("Duplicated a core")

	return core
end, "Data")

function sc_debug(msg) // Show a debug message
	if SpaceCombat_debug then
		print(msg)
	end
end

function sc_entdebug(ent)
	if !IsValid(ent) then return "invalid ent" end
	local enttype = ent:GetClass() or "unknown"
	local entmodel = ent:GetModel() or "unknown"
	return ent:EntIndex() .. " (" .. enttype .. "=" .. entmodel .. ")"
end

function sc_debug_table(tbl) // Show a debug table
	if SpaceCombat_debug then
		PrintTable(tbl)
	end
end

function sc_cvar_bool(cvar) // Returns convar bool
	return GetConVar(cvar):GetBool()
end

function sc_valid_ent(ent) // Used to determine if its a valid candidate for core related things
	if !IsValid(ent) then return false end
	return !ent:IsPlayer() and !ent:IsNPC() and !ent:IsWeapon()
end

function sc_isCore(ent)
	if !IsValid(ent) then return false end
	return ent:GetClass() == "ship_core"
end

function SC_SetHealth(ent, hp)
	ent.SC_Health = hp
	ent:SetNetworkedFloat("SC_Health", hp)
end

function SC_SetMaxHealth(ent, max)
	ent.SC_MaxHealth = max
	ent:SetNetworkedFloat("SC_MaxHealth", max)
end

function SC_SetImmune(ent, immune)
	ent.SC_Immune = immune
	ent:SetNetworkedBool("SC_Immune", immune)
end

function SC_DamageTable(em, exp, kin, therm) // Returns a damage table
	return { EM = em, EXP = exp, KIN = kin, THERM = therm }
end

function SC_TotalDamage(hp, damage, resistance)
	damage = SC_ResistDamage(damage, resistance)

	local newHP = hp - damage.EM
	if newHP < 0 then
		damage.EM = -newHP
		return newHP, SC_UnresistDamage(damage, resistance)
	else
		damage.EM = 0
		newHP = newHP - damage.EXP
		if newHP < 0 then
			damage.EXP = -newHP
			return newHP, SC_UnresistDamage(damage, resistance)
		else
			damage.EXP = 0
			newHP = newHP - damage.KIN
			if newHP < 0 then
				damage.KIN = -newHP
				return newHP, SC_UnresistDamage(damage, resistance)
			else
				damage.KIN = 0
				newHP = newHP - damage.THERM
				if newHP < 0 then
					damage.THERM = -newHP
					return newHP, SC_UnresistDamage(damage, resistance)
				else
					return newHP, SC_DamageTable(0, 0, 0, 0)
				end
			end
		end
	end
end

local function ResToPerc(res)
	return 100 - res * 100
end

function SC_ResistDamage(damage, resistance)
	return SC_DamageTable(damage.EM * (1 - resistance.EM), damage.EXP * (1 - resistance.EXP), damage.KIN * (1 - resistance.KIN), damage.THERM * (1 - resistance.THERM))
end

function SC_UnresistDamage(damage, resistance)
	return SC_DamageTable(damage.EM / ResToPerc(resistance.EM) * 100, damage.EXP / ResToPerc(resistance.EXP) * 100, damage.KIN / ResToPerc(resistance.KIN) * 100, damage.THERM / ResToPerc(resistance.THERM) * 100)
end

function SC_AddDamages(...)
	local result = SC_DamageTable(0, 0, 0, 0)
	for _,tbl in pairs({...}) do
		if tbl.EM then result.EM = result.EM + tbl.EM end
		if tbl.EXP then result.EXP = result.EXP + tbl.EXP end
		if tbl.KIN then result.KIN = result.KIN + tbl.KIN end
		if tbl.THERM then result.THERM = result.THERM + tbl.THERM end
	end
	return result
end

function SC_GetLinkedProps(ent, result)
	local result = result or {}

	if !IsValid(ent) then return end
	if result[ent] then return end
	result[ent] = ent

	for _,con in ipairs(constraint.GetTable(ent)) do
		for _,conEnt in pairs(con.Entity) do
			SC_GetLinkedProps(conEnt.Entity, result)
		end
	end
	return result
end

function SC_GetMatType(ent)	// Bad way to find material type but appears to be the only way there is
	local tr = util.TraceLine({ 
		start = ent:OBBMins() + ent:GetPos(),
		endpos = ent:OBBMaxs() + ent:GetPos(),
	})
	return tr.MatType 
end

function SC_GetHealthMultiplier(ent) // Get entity material multiplier
	local mat = SC_GetMatType(ent)
	return MatMultiplier[mat] or 0.5
end

function SC_GetVolume(ent)
	if !IsValid(ent) then return 0 end
	
	local phys = ent:GetPhysicsObject()
	if !phys or !phys:IsValid() then return 0 end

	return phys:GetVolume() or 0
end

function SC_CalcHealth(ent) // Calculate health of a non cored prop
	local volume = SC_GetVolume(ent)
	local multiplier = SC_GetHealthMultiplier(ent)
	return math.Round(volume^0.515*multiplier)
end

function SC_InitEnt(ent) // Called when a new ent is spawned
	if not IsValid(ent) then return end
	
	local hp = SC_CalcHealth(ent)
	if hp > SC_MaxPropHealth then
		hp = SC_MaxPropHealth	
	elseif hp < SC_MinPropHealth then
		hp = SC_MinPropHealth
	end
	SC_SetHealth(ent, hp)
	SC_SetMaxHealth(ent, hp)
end

function SC_KillEnt(ent, dmginfo) // Called when a ent gets destroyed as a result of damage
	if not IsValid(ent) then return end

	if ent:GetClass() == "ship_core" then
		SC_SetImmune(ent, true)

		local data = EffectData()
		data:SetMagnitude(1)
		data:SetOrigin(ent:GetPos())
		data:SetScale(ent.SC_Size)
		//util.Effect("sc_core_explode", data, true, true)
		util.Effect("breachsplode", data, true, true)

		for _,prop in pairs(ent.LinkedProps) do
			if IsValid(prop) then
				if (string.find(prop:GetClass(), "prop")) then
					SC_SetImmune(prop, true)
					constraint.RemoveAll(prop)
					prop:SetSolid(SOLID_VPHYSICS)
					prop:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
					prop:DrawShadow(false)

					timer.Simple(math.random()*5, function()
						if IsValid(prop) then
							local data2 = EffectData()
							data2:SetOrigin(prop:GetPos())
							util.Effect("Explosion", data2, true, true)
							util.BlastDamage(prop, ent, prop:GetPos(), 300, 250)
							prop:Remove()
						end
					end)

					local physobj = prop:GetPhysicsObject()
					if(physobj:IsValid()) then
						physobj:Wake()
						physobj:EnableMotion(true)
						local mass = physobj:GetMass() or 1
						physobj:ApplyForceCenter(Vector(math.random(mass*-250, mass*250), math.random(mass*-250, mass*250), math.random(mass*-250, mass*250)))
					end
				elseif prop:GetClass() != "ship_core" then
					constraint.RemoveAll(prop)
					prop:Remove()
				end
			end
		end

		//util.BlastDamage(ent, ent, ent:GetPos(), ent.SC_Size, ent.Cap.CAP)
		constraint.RemoveAll(ent)

		ent:SetNotSolid(true)
		ent:SetMoveType(MOVETYPE_NONE)
		ent:SetNoDraw(true)
		timer.Simple(10, function() if IsValid(ent) then ent:Remove() end end)
	else
		local data = EffectData()
		data:SetOrigin(ent:GetPos())
		util.Effect("Explosion", data, true, true)

		constraint.RemoveAll(ent)
		ent:Remove()
	end
end

function SC_ApplyDamage(ent, damage, dmginfo) // Apply damage to an entity
	if not IsValid(ent) then return end
	if ent.SC_Immune then
		sc_debug("Entity " .. sc_entdebug(ent) .. " is immune and doesn't take damage")
		return
	end

	if IsValid(ent.SC_CoreEnt) or ent:GetClass() == "ship_core" then
		sc_debug("Entity " .. sc_entdebug(ent) .. " with core " .. sc_entdebug(ent.SC_CoreEnt) .. " got hit")
		ent.SC_CoreEnt:ApplyDamage(ent, damage, dmginfo)
	else
		local totalDamage = damage.EM + damage.EXP + damage.KIN + damage.THERM

		if !ent.SC_Health then
			if ent:IsPlayer() then
				ent:TakeDamage(totalDamage, dmginfo)
				return
			elseif ent:GetClass() == "shield" then
				sc_debug("Im a shield and im not implemented")
				return
			elseif ent:GetClass() == "sga_shield" then
				sc_debug("Im a sga_shield and im not implemented")
				return
			else
				SC_InitEnt(ent)
			end
		end

		if ent.SC_Health > totalDamage then			
			SC_SetHealth(ent, ent.SC_Health - totalDamage)
			sc_debug("Entity " .. sc_entdebug(ent) .. " took " .. totalDamage .. " damage and now has " .. ent.SC_Health .. "hp")
		else
			sc_debug("Entity " .. sc_entdebug(ent) .. " took " .. totalDamage .. " damage and was destroyed")
			SC_KillEnt(ent, dmginfo)
		end
	end
end

function SC_EntityCreated(ent) // Used to filter out new spawned entities
	timer.Simple(0.1, function()
		if !IsValid(ent) then return end
		if !sc_valid_ent(ent) or !ent:GetPhysicsObject():IsValid() then
			SC_SetImmune(ent, true)
			return
		end
		if ent:GetSolid() == SOLID_NONE or ent:GetClass() == "sent_anim" or ent:GetClass() == "gmod_ghost" or ent:GetClass() == "ship_core" then return end
		if ent.SC_Health then return end
		if CurTime() < 10 then
			SC_SetImmune(ent, true)
			sc_debug("Entity " .. sc_entdebug(ent) .. " is most likely a world entity and was granted immunity")
		else
			SC_InitEnt(ent)
			sc_debug("Entity " .. sc_entdebug(ent) .. " was initialized with " .. ent.SC_Health .. "hp")
		end
	end)
end
hook.Add("OnEntityCreated", "SC_EntityCreated", SC_EntityCreated)

function SC_PostInit()
	timer.Simple(5, function() // We want this to run after everything else
		sc_debug("Checking for world entities that need to be made immune")
		for _,ent in ipairs(ents.GetAll()) do
			SC_SetImmune(ent, true)
		end
	end)
end
hook.Add("InitPostEntity", "SC_PostInit", SC_PostInit)

function SC_EntityTakeDamage(ent, dmginfo) // Used to filter out damage to entities
	if !sc_valid_ent(ent) then return end
	if !ent.SC_CoreEnt and !sc_cvar_bool("scvar_propdmg") then return end
	if ent.SC_Immune then
		sc_debug("Entity " .. sc_entdebug(ent) .. " is immune and doesn't take damage")
		return
	end
	if !ent.SC_Health then SC_InitEnt(ent) end

	local dmgtype = dmginfo:GetDamageType()
	if(dmginfo:IsBulletDamage() || dmgtype == DMG_CLUB) then
		sc_debug("Entity damaged by bullet")
		SC_ApplyDamage(ent, SC_DamageTable(0, 0, dmginfo:GetDamage(), 0), dmginfo)
	elseif(dmginfo:IsExplosionDamage()) then
		sc_debug("Entity damaged by explosion")
		SC_ApplyDamage(ent, SC_DamageTable(0, dmginfo:GetDamage(), 0, 0), dmginfo)
	elseif(dmginfo:IsFallDamage() || dmgtype == DMG_CRUSH) then
		if sc_cvar_bool("scvar_physdmg") then
			sc_debug("Entity damaged by physics")
			SC_ApplyDamage(ent, SC_DamageTable(0, 0, dmginfo:GetDamage(), 0), dmginfo)
		else
			sc_debug("Physics damage is disabled")
		end
	else
		sc_debug("Entity damaged by " .. dmgtype)	
	end
end
hook.Add("EntityTakeDamage", "SC_EntityTakeDamage", SC_EntityTakeDamage)

net.Receive("sc_set_name", function(len, ply)
	local core = net.ReadEntity()
	if !sc_isCore(core) or !IsValid(ply) then return end
	if core.SC_Owner != ply then return end
	
	core.SC_Name = net.ReadString() or "Ship Core"
end)