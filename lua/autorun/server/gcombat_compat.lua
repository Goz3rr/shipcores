COMBATDAMAGEENGINE = 1

if !gcombat then gcombat = {} end

function DoGcombatExplosion(position, radius, damage)
	local damaged = ents.FindInSphere(position, radius)
	local hitentstable = {}

	for _,i in pairs(damaged) do
		local distance = (i:NearestPoint(position) - position):Length()
		local falloff = 1 - distance/radius
		if falloff < 0 then falloff = 0 end
		local dmg = damage * falloff / 3

		local trace = { 
			start = position,
			endpos = i:NearestPoint(position)
		}
		local tr = util.TraceLine(trace)

		if !tr.HitWorld and IsValid(tr.Entity) then
			if i.SC_CoreEnt then
				table.insert(hitentstable, { ent=i, f=falloff })
			else
				SC_ApplyDamage(i, SC_DamageTable(0, dmg, dmg, dmg))
			end
		end
	end

	local alreadyhit = {}
	for _,i in pairs(hitentstable) do
		if !table.HasValue(alreadyhit, i.ent.SC_CoreEnt) then
			local falloff = i.f
			local dmg = damage * falloff / 3
			SC_ApplyDamage(i.ent, SC_DamageTable(0, dmg, dmg, dmg))
			table.insert(alreadyhit, i.ent.SC_CoreEnt)
		end
	end
end

function DoGcombatDamage(entity, damage, src)
	if !IsValid(entity) then return end
	local dmg = damage/3

	if entity:GetClass() == "shield" then
		entity.Parent.Strength = math.Clamp(entity.Parent.Strength-((((damage/2)/SC_SG_Shield_MaxDamage())*100)/(entity.Parent.StrengthMultiplier[1]*entity.Parent.StrengthConfigMultiplier)),0,100)
		entity:Hit(entity, src, 1.0)
	else
		SC_ApplyDamage(entity, SC_DamageTable(0, dmg, dmg, dmg))
	end
end

function gcombat.hcgexplode(position, radius, damage, pierce)
	DoGcombatExplosion(position, radius, damage)
end
cbt_hcgexplode = gcombat.hcgexplode

function gcombat.nrgexplode(position, radius, damage, pierce)
	DoGcombatExplosion(position, radius, damage)
end
cbt_nrgexplode = gcombat.nrgexplode

function gcombat.nrghit(entity, damage, pierce, src, dest)
	DoGcombatDamage(entity, damage, src)
end
cbt_dealnrghit = gcombat.nrghit

function gcombat.hcghit(entity, damage, pierce, src, dest)
	DoGcombatDamage(entity, damage, src)
end
cbt_dealhcghit = gcombat.hcghit

function gcombat.devhit(entity, damage, pierce)
	DoGcombatDamage(entity, damage, src)
end
cbt_dealdevhit = gcombat.devhit

function gcombat.applyheat(ent, temp)
	--nothing. this is kept here so that everything that used to use heat doesn't break.
end
cbt_applyheat = gcombat.applyheat


function gcombat.emitheat(position, radius, temp, own)
	--nothing. this is kept here so that everything that used to use heat doesn't break.
end
cbt_emitheat = gcombat.emitheat